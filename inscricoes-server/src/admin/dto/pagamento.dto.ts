import { IsBoolean } from 'class-validator';

export class PagamentoPayload {
  @IsBoolean({ message: 'Pagamento deve ser informado.' })
  readonly pagamentoRealizado: boolean;
}
