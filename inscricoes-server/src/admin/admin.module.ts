import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { RegistrationsModule } from '../registrations/registrations.module';

@Module({
  imports: [
    RegistrationsModule,
  ],
  controllers: [AdminController],
  providers: [],
})
export class AdminModule {}
