import { Controller, Get, Body, Param, Put, UseGuards, SetMetadata } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { RegistrationsService } from '../registrations/registrations.service';
import { Registration } from '../registrations/registrations.entity';
import { PagamentoPayload } from './dto/pagamento.dto';
import { Roles } from '../admin/admin.decorator';
import { AdminGuard } from '../admin/admin.guard';

@Controller('admin')
@UseGuards(AuthGuard('jwt'))
export class AdminController {
  constructor(
    private readonly registrationService: RegistrationsService,
  ) {}

  @Get('/registrations')
  @UseGuards(AdminGuard)
  getRegistrations(): Promise<Registration[]> {
    return this.registrationService.getAllRegistrations();
  }

  @Put('/registrations/:id')
  informarPagamento(@Param('id') id, @Body() payload: PagamentoPayload): Promise<UpdateResult> {
    return this.registrationService.setPayment(id, payload);
  }
}
