import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { EmailService } from '../email/email.service';
import { User } from '../users/users.entity';
import { ResetPasswordPayload } from '../users/dto/reset-password.dto';
import * as generator from 'generate-password';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly emailService: EmailService,
    private readonly jwtService: JwtService,
  ) { }

  async validateUser(email: string, pass: string): Promise<any> {
    // validar acesso do administrador.
    if ((email === process.env.ADMIN_USER) && (pass === process.env.ADMIN_PASSWORD)) {
      const admin = { email, roles: ['admin'] };
      return admin;
    }

    const user: User = await this.usersService.findUserByEmail(email);

    if (!user) {
      return null;
    }

    const isValid = await this.usersService.validatePassword(user.senha, pass);

    if (!isValid) {
      return null;
    }

    const { senha, ...result } = user;
    return result;
  }

  async login(user: User) {
    return {
      access_token: this.jwtService.sign(user),
      alterarSenha: user.alterarSenha,
      email: user.email,
    };
  }

  async resetPassword(payload: ResetPasswordPayload) {
    const user = await this.usersService.findUserByEmail(payload.email);

    if (!user) {
      throw new HttpException('Usuário não encontrado.', HttpStatus.NOT_FOUND);
    }

    const newPassword = generator.generate({
      excludeSimilarCharacters: true,
      numbers: true,
      length: 10,
    });

    await this.usersService.updatePassword(user.id, {
      ...user,
      senha: newPassword,
      alterarSenha: true,
    });

    return this.emailService.sendResetPasswordEmail(user.email, newPassword);
  }

}
