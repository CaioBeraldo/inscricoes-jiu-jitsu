import { Controller, Post, Req, UseGuards, Body, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ResetPasswordPayload } from '../users/dto/reset-password.dto';

@Controller('auth')
export class AuthController {
  /**
   * Inject controller dependencies.
   * @param authService injected auth services.
   */
  constructor(private readonly authService: AuthService) { }

  @Post('/')
  @UseGuards(AuthGuard('local'))
  login(@Req() req) {
    return this.authService.login(req.user);
  }

  @Put('/')
  resetPassword(@Body() payload: ResetPasswordPayload) {
    return this.authService.resetPassword(payload);
  }
}
