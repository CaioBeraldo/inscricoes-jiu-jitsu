import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { RegistrationsModule } from './registrations/registrations.module';
import { CategoryModule } from './category/category.module';
import { GenreModule } from './genre/genre.module';
import { AbsoluteModule } from './absolute/absolute.module';
import { BeltModule } from './belt/belt.module';
import { TeamModule } from './team/team.module';
import { EmailService } from './email/email.service';
import { AdminModule } from './admin/admin.module';

@Module({
  imports: [
    DatabaseModule,
    AuthModule,
    UsersModule,
    RegistrationsModule,
    CategoryModule,
    GenreModule,
    AbsoluteModule,
    BeltModule,
    TeamModule,
    AdminModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppModule {}
