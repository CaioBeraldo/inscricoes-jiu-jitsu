import { createConnection } from 'typeorm';

export const databaseProvider = [
  {
    provide: process.env.DATABASE_PROVIDER,
    useFactory: async () => {
      const connection = await createConnection({
        type: 'mysql',
        host: process.env.DATABASE_HOST,
        port: Number(process.env.DATABASE_PORT),
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        entities: [
          `${__dirname}/../**/*.entity{.ts,.js}`,
        ],
        synchronize: true,
        debug: false,
        migrationsTableName: 'migrations',
        migrations: [`${__dirname}/../_migrations/*.js`],
        cli: {
          migrationsDir: `${__dirname}/../_migrations`,
        },
      });

      await connection.runMigrations();

      return connection;
    },
  },
];
