import { Controller, Get } from '@nestjs/common';
import { BeltService } from './belt.service';

@Controller('belt')
export class BeltController {
  constructor(private readonly beltService: BeltService) {}

  @Get()
  getBelts() {
    return this.beltService.getBelts();
  }
}
