import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm/repository/Repository';
import { Belt } from './belt.entity';

@Injectable()
export class BeltService {
  constructor(
      @Inject('BELT_REPOSITORY')
      private readonly beltRepository: Repository<Belt>,
  ) { }

  getBelt(id) {
    return this.beltRepository.findOne(id);
  }

  getBelts() {
    return this.beltRepository.find();
  }
}
