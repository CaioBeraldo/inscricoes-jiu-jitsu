import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Belt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  cor: string;
}
