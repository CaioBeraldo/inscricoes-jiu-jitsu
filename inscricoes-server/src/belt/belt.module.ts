import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { BeltController } from './belt.controller';
import { BeltService } from './belt.service';
import { beltProvider } from './belt.provider';

const providers = [
  ...beltProvider,
  BeltService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [BeltController],
  exports: providers
})
export class BeltModule {}
