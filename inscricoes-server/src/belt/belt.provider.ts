import { Connection } from 'typeorm';
import { Belt } from './belt.entity';

export const beltProvider = [
  {
    provide: 'BELT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Belt),
    inject: [process.env.DATABASE_PROVIDER],
  },
];
