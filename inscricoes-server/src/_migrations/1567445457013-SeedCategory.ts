import {MigrationInterface, QueryRunner, getRepository} from 'typeorm';
import { CategoriaSeed } from './seed/categoria';
import { Category } from '../category/category.entity';
import { Genre } from '../genre/genre.entity';

export class SeedCategory1567445457013 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    const categoryRepository = getRepository(Category);
    const genreRepository = getRepository(Genre);

    const categories = CategoriaSeed;

    const categoriesSeedPromise: Promise<Category>[] = categories.map(async (category) => {
      const genre = await genreRepository.findOne(category.genero);

      const result: Category = {
        id: category.id,
        nome: category.nome,
        idade: category.idade,
        peso: category.peso,
        genero: genre,
      }

      return result
    })

    categoryRepository.save(await Promise.all(categoriesSeedPromise));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
