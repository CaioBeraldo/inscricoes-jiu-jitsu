import {MigrationInterface, QueryRunner, getRepository} from 'typeorm';
import { Absolute } from '../absolute/absolute.entity';
import { Genre } from '../genre/genre.entity';
import { AbsolutoSeed } from './seed/absoluto';

export class SeedAbsolute1567447128225 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    const absoluteRepository = getRepository(Absolute);
    const genreRepository = getRepository(Genre);

    const absolutes = AbsolutoSeed;

    const absolutesSeedPromise: Promise<Absolute>[] = absolutes.map(async (absolute) => {
      const genre = await genreRepository.findOne(absolute.genero);

      const result: Absolute = {
        id: absolute.id,
        nome: absolute.nome,
        genero: genre,
      }

      return result
    })

    absoluteRepository.save(await Promise.all(absolutesSeedPromise));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
