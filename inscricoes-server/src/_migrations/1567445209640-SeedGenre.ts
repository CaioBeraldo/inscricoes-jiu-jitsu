import {MigrationInterface, QueryRunner, getRepository} from 'typeorm';
import { GeneroSeed } from './seed/genero';
import { Genre } from '../genre/genre.entity';

export class SeedGenre1567445209640 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    getRepository(Genre).save(GeneroSeed);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
