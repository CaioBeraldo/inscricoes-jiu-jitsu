export const BeltSeed = [
  { id: 1, cor: 'Branca' },
  { id: 2, cor: 'Cinza' },
  { id: 3, cor: 'Amarela' },
  { id: 4, cor: 'Laranja' },
  { id: 5, cor: 'Verde' },
  { id: 6, cor: 'Azul' },
  { id: 7, cor: 'Roxa' },
  { id: 8, cor: 'Marrom' },
  { id: 9, cor: 'Preta' }
];
