import {MigrationInterface, QueryRunner, getRepository} from 'typeorm';
import { Belt } from '../belt/belt.entity';
import { BeltSeed } from './seed/faixas';

export class SeedBelt1567448172040 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    getRepository(Belt).save(BeltSeed);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
