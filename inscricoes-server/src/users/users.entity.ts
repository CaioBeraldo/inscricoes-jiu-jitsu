import { Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne } from 'typeorm';
import { Team } from '../team/team.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 200 })
  senha: string;

  @Index({ unique: true })
  @Column({ length: 300 })
  email: string;

  @Column({ length: 100 })
  nome: string;

  @ManyToOne(type => Team)
  equipe: Team;

  @Column({ length: 15 })
  celular: string;

  @Column({ length: 50 })
  cidade: string;

  @Column({ length: 2 })
  estado: string;

  @Column()
  alterarSenha: boolean;
}
