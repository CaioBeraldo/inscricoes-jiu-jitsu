import { Controller, Get, Post, Put, Param, Body, UseGuards, Req, Res, HttpException, HttpStatus } from '@nestjs/common';
import { sendUnauthorizedResponse, validateUserRequestById } from '../auth/unauthorized.response';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserPayload } from './dto/users.create.dto';
import { UsersService } from './users.service';
import { TeamService } from '../team/team.service';
import { NewPasswordPayload } from './dto/new-password.dto';

@Controller('users')
export class UsersController {
  /**
   * Inject controller dependencies.
   * @param {UsersService} usersService injected users services.
   */
  constructor(
    private readonly usersService: UsersService,
    private readonly teamService: TeamService,
  ) { }

  @Post('/')
  async createUser(@Body() user: CreateUserPayload) {
    const duplicated = await this.usersService.findUserByEmail(user.email);

    if (!!duplicated) {
      throw new HttpException('E-mail informado já está cadastrado.', 400);
    }

    let [equipe] = await this.teamService.getTeamByName(user.equipe);

    if (!equipe) {
      equipe = await this.teamService.createTeam({ nome: user.equipe });
    }

    const newUser = { ...user, equipe };

    return this.usersService.createUser(newUser);
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  getUser(@Req() req, @Res() res, @Param('id') id) {
    if (validateUserRequestById(id, req)) {
      sendUnauthorizedResponse(res);
      return;
    }

    return this.usersService.getUser(id);
  }

  @Put('/')
  @UseGuards(AuthGuard('jwt'))
  async updateUserPassword(@Req() req, @Body() payload: NewPasswordPayload) {
    const user = await this.usersService.findUserByEmail(req.user.email);

    if (!req.user.alterarSenha) {
      throw new HttpException('A senha não pode ser alterada.', HttpStatus.UNAUTHORIZED);
    }

    const updated = await this.usersService.updatePassword(user.id, {
      ...user,
      senha: payload.senha,
      alterarSenha: false,
    });

    return { email: user.email };
  }

  @Get('/')
  getUsers() {
    return this.usersService.getUsers();
  }

  /*
  @Delete('/:id')
  deleteUser(@Req() req, @Res() res, @Param('id') id) {
    if (validateUserRequestById(id, req)) {
      sendUnauthorizedResponse(res);
      return;
    }

    return this.usersService.deleteUser(id);
  }
  */
}
