import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { userProvider } from './users.provider';
import { DatabaseModule } from '../database/database.module';
import { TeamModule } from '../team/team.module';

const providers = [
  ...userProvider,
  UsersService,
];

@Module({
  providers,
  imports: [
    DatabaseModule,
    TeamModule,
  ],
  controllers: [UsersController],
  exports: providers,
})
export class UsersModule {}
