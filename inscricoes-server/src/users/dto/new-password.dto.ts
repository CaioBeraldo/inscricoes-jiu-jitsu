import { IsNotEmpty, MinLength } from 'class-validator';

export class NewPasswordPayload {
  @IsNotEmpty({ message: 'Senha deve ser informada.' })
  @MinLength(5, { message: 'Senha deve conter no mínimo 5 caracteres.' })
  readonly senha: string;
}
