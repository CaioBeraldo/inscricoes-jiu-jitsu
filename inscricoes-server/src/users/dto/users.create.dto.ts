import { IsNotEmpty, IsEmail, MinLength, Length } from 'class-validator';

export class CreateUserPayload {
  @IsEmail()
  @IsNotEmpty({ message: 'E-mail deve ser informado.' })
  readonly email: string;
  @IsNotEmpty({ message: 'Senha deve ser informada.' })
  @MinLength(5, { message: 'Senha deve conter no mínimo 5 caracteres.' })
  readonly senha: string;
  @IsNotEmpty({ message: 'Nome deve ser informado.' })
  readonly nome: string;
  @IsNotEmpty({ message: 'Nome da equipe deve ser informado.' })
  readonly equipe: string;
  @IsNotEmpty({ message: 'Celular deve ser informado.' })
  readonly celular: string;
  @IsNotEmpty({ message: 'Cidade deve ser informada.' })
  readonly cidade: string;
  @Length(2, 2, { message: 'Estado informado é inválido (Ex.: SP, SC, MG).' })
  @IsNotEmpty({ message: 'Estado deve ser informado.' })
  readonly estado: string;
}
