import { IsNotEmpty, IsEmail, Length } from 'class-validator';

export class ResetPasswordPayload {
  @IsEmail()
  @IsNotEmpty({ message: 'E-mail deve ser informado.' })
  readonly email: string;
}
