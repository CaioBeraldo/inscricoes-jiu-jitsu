import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as mailer from 'nodemailer';
import * as smtpTransport from 'nodemailer-smtp-transport';

@Injectable()
export class EmailService {
  private transporter;
  private from;

  constructor() {
    this.initialize();
  }

  async initialize() {
    let transporterOptions;

    if (!process.env.PRODUCTION) {
      const testAccount = await mailer.createTestAccount();

      this.from = `"LRJJ" <${testAccount.user}>`;

      transporterOptions = {
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: testAccount.user, // generated ethereal user
          pass: testAccount.pass, // generated ethereal password
        },
      };
    } else {
      this.from = `"LRJJ" <${process.env.EMAIL_USER}>`;

      transporterOptions = smtpTransport({
        service: process.env.EMAIL_SERVICE,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASS,
        },
      });
    }

    this.transporter = mailer.createTransport(transporterOptions);
  }

  async sendResetPasswordEmail(email: string, password: string): Promise<any> {
    const info = await this.transporter.sendMail({
      from: this.from,
      subject: `Recuperação de Senha`,
      to: email,
      html: `<style>
        .lrjj-email {width: 100% !important;height: 100% !important;
          background-color: rgb(11.7%, 14.4%, 11.7%) !important;
          color: white !important;padding: 10px !important;}
        .lrjj-title {color: lightgreen !important;text-decoration: underline !important;}
        .lrjj-header {color: white !important;}
        .lrjj-link {color: lightgreen !important;text-decoration: underline !important;}
        </style>
        <div class="lrjj-email">
          <h1 class="lrjj-title">Recuperação de Senha</h1>
          <h2 class="lrjj-header">Ops. Esqueceu a senha?</h2>
          <p>Essa é a sua nova senha: <a href="https://lrjj.com.br" class="lrjj-link">${password}</a></p>
        </div>`,
    });

    if (!process.env.PRODUCTION) {
      console.log(info.messageId, 'Message sent');
      console.log(mailer.getTestMessageUrl(info), 'Preview URL');
    }

    if (info.accepted.length === 0) {
      throw new HttpException('Não foi possível enviar o e-mail.', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return info;
  }
}
