import { IsNotEmpty } from 'class-validator';

export class RegistrationPayload {
  @IsNotEmpty({ message: 'Nome deve ser informado.' })
  readonly nome: string;
  @IsNotEmpty({ message: 'Idade deve ser informada.' })
  readonly idade: number;
  @IsNotEmpty({ message: 'Peso deve ser informado.' })
  readonly peso: number;
  @IsNotEmpty({ message: 'Equipe deve ser selecionada.' })
  readonly equipe: string;
  @IsNotEmpty({ message: 'Sexo deve ser selecionado.' })
  readonly genero: number;
  @IsNotEmpty({ message: 'Categoria deve ser selecionada.' })
  readonly categoria: number;
  @IsNotEmpty({ message: 'Absoluto deve ser selecionado.' })
  readonly absoluto: number;
  @IsNotEmpty({ message: 'Faixa deve ser selecionada.' })
  readonly faixa: number;
}
