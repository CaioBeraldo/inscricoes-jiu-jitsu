import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne } from 'typeorm';
import { Genre } from '../genre/genre.entity';
import { Category } from '../category/category.entity';
import { Absolute } from '../absolute/absolute.entity';
import { Belt } from '../belt/belt.entity';
import { User } from '../users/users.entity';
import { Team } from '../team/team.entity';
import { IsNotEmpty } from 'class-validator';

@Entity()
export class Registration {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 200 })
  nome: string;

  @Column()
  idade: number;

  @Column()
  peso: number;

  @ManyToOne(type => Team)
  equipe: Team;

  @ManyToOne(type => Genre)
  genero: Genre;

  @ManyToOne(type => Category)
  categoria: Category;

  @ManyToOne(type => Absolute)
  absoluto: Absolute;

  @ManyToOne(type => Belt)
  faixa: Belt;

  @IsNotEmpty()
  @ManyToOne(type => User)
  usuario: User;

  @Column()
  pagamentoRealizado: boolean;
}
