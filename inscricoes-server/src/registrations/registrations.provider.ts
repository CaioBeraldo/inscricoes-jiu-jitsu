import { Connection } from 'typeorm';
import { Registration } from './registrations.entity';

export const registrationsProvider = [
  {
    provide: 'REGISTRATIONS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Registration),
    inject: [process.env.DATABASE_PROVIDER],
  },
];
