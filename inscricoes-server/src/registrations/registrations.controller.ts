import { Controller, Get, Post, Body, UseGuards, Req, Param, Put, Delete, HttpException, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RegistrationsService } from './registrations.service';
import { RegistrationPayload } from './dto/registration.dto';
import { CategoryService } from '../category/category.service';
import { AbsoluteService } from '../absolute/absolute.service';
import { GenreService } from '../genre/genre.service';
import { BeltService } from '../belt/belt.service';
import { TeamService } from '../team/team.service';
import { Registration } from './registrations.entity';
import { Roles } from '../admin/admin.decorator';
import { UsersService } from '../users/users.service';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';

@Controller('')
export class RegistrationsController {
  constructor(
    private readonly registrationService: RegistrationsService,
    private readonly categoryService: CategoryService,
    private readonly absoluteService: AbsoluteService,
    private readonly genreService: GenreService,
    private readonly beltService: BeltService,
    private readonly teamService: TeamService,
    private readonly userService: UsersService,
  ) {}

  @Post('/registrations')
  @UseGuards(AuthGuard('jwt'))
  async register(@Req() req, @Body() registration: RegistrationPayload) {
    const genero = await this.genreService.getGenre(registration.genero);
    const categoria = await this.categoryService.getCategory(registration.categoria);
    const absoluto = await this.absoluteService.getAbsolute(registration.absoluto);
    const faixa = await this.beltService.getBelt(registration.faixa);
    const equipe = await this.teamService.getTeam(registration.equipe);
    const usuario = await this.userService.getUser(req.user.id);

    if (!usuario) {
      throw new HttpException('Usuário não autorizado.', HttpStatus.UNAUTHORIZED);
    }

    const newRegistration: Registration = {
      ...registration,
      genero,
      categoria,
      absoluto,
      faixa,
      usuario,
      equipe,
      pagamentoRealizado: false,
      id: 0,
    };

    return await this.registrationService.register(newRegistration);
  }

  @Put('/registrations/:id')
  @UseGuards(AuthGuard('jwt'))
  async updateRegistration(@Req() req, @Param('id') id, @Body() registration: RegistrationPayload): Promise<UpdateResult> {
    const updated = await this.registrationService.getRegistration(id);
    if (req.user.id !== updated.usuario.id) {
      throw new HttpException('Usuário não autorizado.', HttpStatus.UNAUTHORIZED);
    }

    return await this.registrationService.updateRegistration(id, registration);
  }

  @Delete('/registrations/:id')
  @UseGuards(AuthGuard('jwt'))
  async deleteRegistration(@Req() req, @Param('id') id): Promise<DeleteResult> {
    const deleted = await this.registrationService.getRegistration(id);
    if (req.user.id !== deleted.usuario.id) {
      throw new HttpException('Usuário não autorizado.', HttpStatus.UNAUTHORIZED);
    }

    return await this.registrationService.deleteRegistration(id);
  }

  @Get('/registrations/:id')
  @UseGuards(AuthGuard('jwt'))
  async getRegistration(@Req() req, @Param('id') id) {
    return await this.registrationService.getRegistration(id);
  }

  @Get('/registrations')
  @UseGuards(AuthGuard('jwt'))
  async getRegistrations(@Req() req) {
    if (!req.user) {
      throw new HttpException('Usuário não autorizado.', HttpStatus.UNAUTHORIZED);
    }

    return await this.registrationService.getRegistrations(req.user.id);
  }

  @Get('/check/category/:category')
  async getCheckCategory(@Param('category') category) {
    return await this.registrationService.getCheckCategory(category);
  }

  @Get('/check/absolute/:absolute')
  async getCheckAbsolute(@Param('absolute') absolute) {
    return await this.registrationService.getCheckAbsolute(absolute);
  }
}
