import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm/repository/Repository';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { Registration } from '../registrations/registrations.entity';
import { PagamentoPayload } from '../admin/dto/pagamento.dto';

@Injectable()
export class RegistrationsService {
  constructor(
    @Inject('REGISTRATIONS_REPOSITORY')
    private readonly registrationRepository: Repository<Registration>,
  ) {}

  register(registration): Promise<Registration> {
    return this.registrationRepository.save(registration);
  }

  async setPayment(id: number, payload: PagamentoPayload): Promise<UpdateResult> {
    const registration = await this.getRegistration(id);
    registration.pagamentoRealizado = payload.pagamentoRealizado;
    return this.registrationRepository.update(id, registration);
  }

  updateRegistration(id, registration): Promise<UpdateResult> {
    return this.registrationRepository.update(id, registration);
  }

  deleteRegistration(id): Promise<DeleteResult> {
    return this.registrationRepository.delete(id);
  }

  getAllRegistrations(): Promise<Registration[]> {
    return this.registrationRepository.find({
      relations: ['equipe', 'genero', 'categoria', 'absoluto', 'faixa', 'usuario'],
    });
  }

  getRegistration(id): Promise<Registration> {
    return this.registrationRepository.findOne({
      relations: ['equipe', 'genero', 'categoria', 'absoluto', 'faixa', 'usuario'],
      where: { id },
    });
  }

  getRegistrations(userId): Promise<Registration[]> {
    return this.registrationRepository.find({
      relations: ['equipe', 'genero', 'categoria', 'absoluto', 'faixa', 'usuario'],
      where: { usuario: { id: userId } },
    });
  }

  getCheckCategory(categoryId): Promise<Registration[]> {
    return this.registrationRepository.find({
      relations: ['equipe', 'categoria', 'absoluto'],
      where: { categoria: { id: categoryId } },
    });
  }

  getCheckAbsolute(absoluteId): Promise<Registration[]> {
    return this.registrationRepository.find({
      relations: ['equipe', 'categoria', 'absoluto'],
      where: { absoluto: { id: absoluteId } },
    });
  }
}
