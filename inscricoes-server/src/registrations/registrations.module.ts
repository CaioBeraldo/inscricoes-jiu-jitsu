import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { RegistrationsController } from './registrations.controller';
import { RegistrationsService } from './registrations.service';
import { registrationsProvider } from './registrations.provider';
import { CategoryModule } from '../category/category.module';
import { AbsoluteModule } from '../absolute/absolute.module';
import { GenreModule } from '../genre/genre.module';
import { BeltModule } from '../belt/belt.module';
import { TeamModule } from '../team/team.module';
import { UsersModule } from '../users/users.module';

const providers = [
  ...registrationsProvider,
  RegistrationsService,
];

@Module({
  providers,
  imports: [
    DatabaseModule,
    CategoryModule,
    AbsoluteModule,
    GenreModule,
    BeltModule,
    TeamModule,
    UsersModule,
  ],
  controllers: [RegistrationsController],
  exports: providers,
})
export class RegistrationsModule {}
