import { Connection } from 'typeorm';
import { Genre } from './genre.entity';

export const genreProvider = [
  {
    provide: 'GENRE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Genre),
    inject: [process.env.DATABASE_PROVIDER],
  },
];
