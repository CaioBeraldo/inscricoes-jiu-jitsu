import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Genre } from './genre.entity';

@Injectable()
export class GenreService {
  constructor(
    @Inject('GENRE_REPOSITORY')
    private readonly genre: Repository<Genre>,
  ) {}

  getGenre(id) {
    return this.genre.findOne(id);
  }

  getGenres() {
    return this.genre.find();
  }
}
