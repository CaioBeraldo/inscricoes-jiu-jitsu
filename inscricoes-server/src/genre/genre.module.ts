import { Module } from '@nestjs/common';
import { GenreService } from './genre.service';
import { GenreController } from './genre.controller';
import { DatabaseModule } from '../database/database.module';
import { genreProvider } from './genre.provider';

const providers = [
  ...genreProvider,
  GenreService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [GenreController],
  exports: providers,
})
export class GenreModule {}
