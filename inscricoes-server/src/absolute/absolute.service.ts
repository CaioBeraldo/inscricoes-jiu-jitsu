import { Injectable, Inject, Param } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Absolute } from './absolute.entity';

@Injectable()
export class AbsoluteService {
  constructor(
    @Inject('ABSOLUTE_REPOSITORY')
    private readonly absolute: Repository<Absolute>,
  ) {}

  getAbsolute(id) {
    return this.absolute.findOne(id);
  }

  getAbsolutes() {
    return this.absolute.find();
  }

  getAbsolutesByGenre(genre) {
    return this.absolute.find({ where: { genero: { id: genre } } });
  }
}
