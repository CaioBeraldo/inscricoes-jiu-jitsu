import { Controller, Get, Param } from '@nestjs/common';
import { AbsoluteService } from './absolute.service';

@Controller('absolute')
export class AbsoluteController {
  constructor(private readonly absolute: AbsoluteService) {}

  @Get('/')
  getAbsolutes() {
    return this.absolute.getAbsolutes();
  }

  @Get('/:genre')
  getAbsolutesByGenre(@Param('genre') genre: number) {
    return this.absolute.getAbsolutesByGenre(genre);
  }
}
