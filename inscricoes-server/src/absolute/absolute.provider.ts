import { Connection } from 'typeorm';
import { Absolute } from './absolute.entity';

export const absoluteProvider = [
  {
    provide: 'ABSOLUTE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Absolute),
    inject: [process.env.DATABASE_PROVIDER],
  },
];
