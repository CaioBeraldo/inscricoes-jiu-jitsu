import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne, ManyToOne } from 'typeorm';
import { Genre } from '../genre/genre.entity';

@Entity()
export class Absolute {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 300 })
  nome: string;

  @ManyToOne(type => Genre)
  @JoinColumn()
  genero: Genre;
}
