import { Module } from '@nestjs/common';
import { AbsoluteService } from './absolute.service';
import { DatabaseModule } from '../database/database.module';
import { AbsoluteController } from './absolute.controller';
import { absoluteProvider } from './absolute.provider';

const providers = [
  ...absoluteProvider,
  AbsoluteService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [AbsoluteController],
  exports: providers,
})
export class AbsoluteModule {}
