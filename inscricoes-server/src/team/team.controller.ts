import { Controller, Get, Param, Post } from '@nestjs/common';
import { TeamService } from './team.service';

@Controller('team')
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  @Get('/')
  getTeams() {
    return this.teamService.getTeams();
  }

}
