import { Connection } from 'typeorm';
import { Team } from './team.entity';

export const teamProvider = [
  {
    provide: 'TEAM_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Team),
    inject: [process.env.DATABASE_PROVIDER],
  },
];
