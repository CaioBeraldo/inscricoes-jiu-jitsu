import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm/repository/Repository';
import { Team } from './team.entity';

@Injectable()
export class TeamService {
  constructor(
    @Inject('TEAM_REPOSITORY')
    private readonly teamRepository: Repository<Team>,
  ) {}

  getTeams() {
    return this.teamRepository.find();
  }

  getTeam(id) {
    return this.teamRepository.findOne(id);
  }

  createTeam(team) {
    return this.teamRepository.save(team);
  }

  getTeamByName(nome) {
    return this.teamRepository.find({
      where: { nome },
    });
  }

}
