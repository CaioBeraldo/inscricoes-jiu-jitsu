import { Module } from '@nestjs/common';
import { TeamService } from './team.service';
import { TeamController } from './team.controller';
import { teamProvider } from './team.provider';
import { DatabaseModule } from '../database/database.module';

const providers = [
  ...teamProvider,
  TeamService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [TeamController],
  exports: providers,
})
export class TeamModule {}
