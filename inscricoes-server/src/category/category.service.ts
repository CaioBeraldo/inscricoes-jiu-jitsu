import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Category } from './category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @Inject('CATEGORY_REPOSITORY')
    private readonly categories: Repository<Category>,
  ) {}

  getCategory(id) {
    return this.categories.findOne(id);
  }

  getCategories() {
    return this.categories.find();
  }

  getCategoriesByGenre(genre: number) {
    return this.categories.find({
      where: { genero: { id: genre }},
    });
  }
}
