import { Entity, PrimaryGeneratedColumn, Column, Index, JoinColumn, OneToOne, ManyToOne } from 'typeorm';
import { Genre } from '../genre/genre.entity';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Genre)
  genero: Genre;

  @Column({ length: 300 })
  nome: string;

  @Column()
  idade: number;

  @Column()
  peso: number;
}
