import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { CategoryService } from './category.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('category')
export class CategoryController {
  constructor(private readonly service: CategoryService) {}

  @Get('/')
  getCategories() {
    return this.service.getCategories();
  }

  @Get('/:genre')
  getCategoriesByGenre(@Param('genre') genre: number) {
    return this.service.getCategoriesByGenre(genre);
  }
}
