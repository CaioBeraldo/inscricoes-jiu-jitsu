import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { DatabaseModule } from '../database/database.module';
import { categoryProvider } from './category.provider';

const providers = [
  ...categoryProvider,
  CategoryService,
];

@Module({
  providers,
  imports: [DatabaseModule],
  controllers: [CategoryController],
  exports: providers,
})
export class CategoryModule {}
