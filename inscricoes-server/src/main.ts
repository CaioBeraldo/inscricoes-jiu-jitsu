import * as dotenv from 'dotenv';

if (!process.env.PRODUCTION) {
  const result = dotenv.config();
  if (result.error) { throw result.error; }
}

import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { json, urlencoded } from 'body-parser';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as session from 'express-session';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const port = process.env.PORT || 3000;

  // allow json requests to endpoints.
  app.use(json());
  // allow url encoded requests to endpoints.
  app.use(urlencoded({ extended: true }));
  // allow cross origin requests to endpoints.
  app.enableCors();
  // session options.
  app.use(session({
    name: 'bHJqai1zZXNzaW9uLWlk',
    secret: 'bHJqai1zZXNzaW9uLXNlY3JldA==',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true },
  }));
  // security options.
  app.use(helmet());
  // enable validation throught class definition in DTO.
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(port, () => {
    Logger.log(`Application is running on http://localhost:${port}`, 'Inscrições');
  });
}
bootstrap();
