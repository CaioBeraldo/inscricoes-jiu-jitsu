# LRJJ

## Analise e explicações

- Se for liberado acesso apenas por campeonato não precisa criar cadastro unificado, por que as informações serão exportadas, caso contrário explicar que pode ser criado um cadastro de campeonato onde o instrutor seleciona qual campeonato serão inscritos os atletas.

## Solicitações para criação do formulário

- Qualquer um pode entrar e criar o cadastro de quem quiser para facilitar.
- Deve ser `responsivo` (deve rodar nas plataformas Android/iOS/Browser).

Verificar a necessidade de criar login e senha visto que qualquer um pode realizar o cadastro.
> Cadastro/Login (do responsável pelos instrutores): E-mail e senha.

Cadastro feito apenas internamente, exibir a listagem das equipes ao digitar no campo e permitir selecionar.
> Cadastro de Equipe (Nome da equipe)

### Inscrição para o campeonato

- Formulário:
  - Nome do Atleta
  - Idade
  - Peso
  - Equipe
  - Sexo
  - Categoria
  - Categoria absoluta

`Abrir categorias com base no sexo (Masculino ou Feminino) - pegar tabela de categorias do banco de dados.`

`Permitir corrigir os cadastros dos atletas.`

Checagem das inscrições
  Não possui modelo ou exemplo, não está funcionando.

Permitir exportação dos dados

## Estrutura final do site

Banner
Formulário
Patrocinadores

[Link do formulário atual](http://lrjj.com.br/lrjj/index.php).

Está hopedado no `Hostgator`.

[Painel de configurações do lrjj](http://lrjj.com.br/cpanel).
