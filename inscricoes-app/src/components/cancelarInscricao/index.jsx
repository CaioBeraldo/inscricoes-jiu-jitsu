import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '../componentes/button';
import { ButtonGroup } from '../componentes/button-group';
import { fetchAPI, options } from '../../store/handlers.api';
import { API_URL } from '../../constants';
import { Message } from '../componentes/message/index';
import { fetchDeleteRegistration } from '../../store/registration/registration.api';

const CancelarInscricao = ({ accessToken, error_message, fetchDeleteRegistration, history, match }) => {
  const [registration, setRegistration] = useState({});

  useEffect(() => {
    fetchAPI(`${API_URL}/registrations/${match.params.id}`, {
      ...options,
      headers: {
        ...options.headers,
        Authorization: `Bearer ${accessToken}`
      },
    })
    .then(setRegistration);
  }, []);

  return (
    <div className="container">
      <div className="titulo">
        <h2>Cancelar Inscrição</h2>
        <p>Deseja cancelar a inscrição do atleta?</p>
      </div>

      <form className="form" id="inscricao">
        {!registration ?
          <p>Carregando...</p> :
          <div>
            <p>Nome: {registration.nome}</p>
            <p>Equipe: {registration.equipe && registration.equipe.nome}</p>
          </div>}
      </form>

      <Message message={error_message} />

      <ButtonGroup>
        <Button label={'Cancelar Inscrição'} type="tertiary" onClick={() => {
          fetchDeleteRegistration(match.params.id, accessToken)
            .then(() => history.push('/minhasInscricoes'));
        }} />
      </ButtonGroup>
      <p className="color-primary">Essa ação não pode ser desfeita.</p>
    </div>
  );
};

const mapStateToProps = state => ({
  accessToken: state.authReducer.accessToken,
  error_message: state.registrationReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchDeleteRegistration,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CancelarInscricao);
