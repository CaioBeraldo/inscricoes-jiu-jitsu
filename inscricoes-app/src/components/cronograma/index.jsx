import React from 'react';
import cronograma from '../../assets/cronograma.pdf';

import './index.scss';

const Cronograma = () => (
  <div className="container">
    <iframe src={cronograma} className="cronograma">
    </iframe>
  </div>
);

export default Cronograma;
