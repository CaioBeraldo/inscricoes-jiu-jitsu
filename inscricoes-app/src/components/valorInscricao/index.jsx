import React from 'react';

import './index.scss';

const ValorInscricao = () => (
  <div className="container">
    <div className="valor-inscricao">
      <h2>Valor das inscrições:</h2>

      <p>Categoria<span>R$ 70,00</span></p>
      <p>Absoluto<span>R$ 90,00</span></p>
      <p>Categoria + Absoluto<span>R$ 90,00</span></p>
      <p>Atletas até 14 anos<span>R$ 45,00</span></p>

      <p><span>* VALOR DEVE SER PAGO NO DIA DO EVENTO!*</span></p>
    </div>
  </div>
);

export default ValorInscricao;
