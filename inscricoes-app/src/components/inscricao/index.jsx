import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input } from '../componentes/input';
import { Button } from '../componentes/button';
import { Select } from '../componentes/select';
import { InputGroup } from '../componentes/input-group';
import { ButtonGroup } from '../componentes/button-group';
import { fetchRegister, fetchUpdateRegistration } from '../../store/registration/registration.api';
import { fetchTeams } from '../../store/team/team.api';
import { options } from '../../store/handlers.api';
import { API_URL } from '../../constants';
import { Message } from '../componentes/message/index';
import { SuccessMessage } from '../componentes/message/success-message';

import './index.css';

const emptyOption = { value: '', title: '' };

const Inscricao = ({ accessToken, teams, match, error_message,
  fetchRegister, fetchUpdateRegistration, fetchTeams }) => {
  const [isRedirectToMyRegistrations, setRedirectToMyRegistrations] = useState(false);
  const [successMessage, setSuccessMessage] = useState(undefined);

  const [nome, setNome] = useState('');
  const [idade, setIdade] = useState('');
  const [peso, setPeso] = useState('');
  const [team, setTeam] = useState('');
  const [genre, setGenre] = useState('');
  const [category, setCategory] = useState('');
  const [absolute, setAbsolute] = useState('');
  const [belt, setBelt] = useState('');

  const [genres, setGenres] = useState([]);
  const [categories, setCategories] = useState([]);
  const [absolutes, setAbsolutes] = useState([]);
  const [belts, setBelts] = useState([]);

  const getTeams = () => [emptyOption, ...teams.map(t => ({ value: t.id, title: t.nome }))]

  useEffect(() => {
    fetchTeams();

    fetch(`${API_URL}/genre`)
      .then(res => res.json())
      .then(data => data.map(g => ({ value: g.id, title: g.genero })))
      .then(array => [emptyOption, ...array])
      .then(setGenres);

    fetch(`${API_URL}/belt`)
      .then(res => res.json())
      .then(data => data.map(b => ({ value: b.id, title: b.cor })))
      .then(array => [emptyOption, ...array])
      .then(setBelts);

    match.params.id && fetch(`${API_URL}/registrations/${match.params.id}`, {
      ...options,
      headers: {
        ...options.headers,
        Authorization: `Bearer ${accessToken}`
      },
    })
    .then(res => res.json())
    .then(data => {
      setNome(data.nome);
      setIdade(data.idade);
      setPeso(data.peso);
      setTeam(data.equipe.id);
      setGenre(data.genero.id);
      setCategory(data.categoria.id);
      setAbsolute(data.absoluto.id);
      setBelt(data.faixa.id);
    });
  }, []);

  useEffect(() => {
    if (!genre) {
      return;
    }

    fetch(`${API_URL}/category/${genre}`)
      .then(res => res.json())
      .then(data => data.map(c => ({ value: c.id, title: c.nome })))
      .then(array => [emptyOption, ...array])
      .then(setCategories)

    fetch(`${API_URL}/absolute/${genre}`)
      .then(res => res.json())
      .then(data => data.map(a => ({ value: a.id, title: a.nome })))
      .then(array => [emptyOption, ...array])
      .then(setAbsolutes)
  }, [genre])

  if (isRedirectToMyRegistrations) {
    return <Redirect to="/minhasInscricoes" />
  }

  return (
    <div className="container">
      <div className="titulo">
        <h2>Formulário de Inscrição</h2>
        <h2 className="color-secondary">Great Champions</h2>
      </div>

      <form className="form" id="inscricao">
        <Input type="text" name="nome" label="Nome do Atleta" value={nome} onChange={setNome} />
        <InputGroup>
          <Input type="text" name="idade" label="Idade" value={idade} onChange={setIdade} mask="999" maskChar={null} />
          <Input type="text" name="peso" label="Peso" value={peso} onChange={setPeso} mask="999" maskChar={null} />
        </InputGroup>
        <Select name="equipe" label="Equipe" options={getTeams()} value={team} onChange={setTeam} />
        <Select name="genero" label="Sexo" options={genres} value={genre} onChange={setGenre} />
        <Select name="categoria" label="Categoria" options={categories.sort((c1, c2) => c1.title > c2.title ? 1 : -1)} value={category} onChange={setCategory} />
        <Select name="absoluto" label="Categoria absoluto" options={absolutes.sort((a1, a2) => a1.title > a2.title ? 1 : -1)} value={absolute} onChange={setAbsolute} />
        <Select name="faixa" label="Faixa" options={belts} value={belt} onChange={setBelt} />
      </form>

      <Message message={error_message} />
      <SuccessMessage message={successMessage} setMessage={setSuccessMessage} />

      <ButtonGroup>
        <Button 
          label={match.params.id ? 'Modificar Inscrição' : 'Confirmar Inscrição'} 
          type="secondary"
          onClick={() => {
            const inscricao = {
              id: match.params.id,
              nome,
              idade,
              peso,
              equipe: team,
              genero: genre,
              categoria: category,
              absoluto: absolute,
              faixa: belt,
            };

            inscricao.id ? 
              fetchUpdateRegistration(inscricao, accessToken).then(() => {
                setRedirectToMyRegistrations(true)
              }) :
              fetchRegister(inscricao, accessToken)
                .then(() => {
                  setSuccessMessage('Inscrição confirmada com sucesso!');
                  setNome('');
                  setIdade('');
                  setPeso('');
                  setTeam('');
                  setGenre('');
                  setCategory('');
                  setAbsolute('');
                  setBelt('');
                });
            }} />
      </ButtonGroup>
    </div>
  )
};

const mapStateToProps = state => ({
  accessToken: state.authReducer.accessToken,
  teams: state.teamsReducer.teams,
  error_message: state.registrationReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchRegister,
  fetchUpdateRegistration,
  fetchTeams,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Inscricao);
