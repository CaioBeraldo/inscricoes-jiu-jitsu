import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Message } from '../componentes/message';
import { SuccessMessage } from '../componentes/message/success-message';
import { ButtonGroup } from '../componentes/button-group';
import { Button } from '../componentes/button';
import { Input } from '../componentes/input';
import { formToJSON } from '../componentes/form/form-to-json';
import { fetchResetPassword } from '../../store/auth/auth.api';

import './index.scss';

const ResetarSenha = ({ error_message, fetchResetPassword }) => {
  const [successMessage, setSuccessMessage] = useState('');

  return (
    <div className="container">
      <div className="titulo">
        <h2>Resetar Senha</h2>
      </div>

      <form className="form" id="reset">
        <Input type="text" name="email" label="E-mail" />
      </form>

      <Message message={error_message} />
      <SuccessMessage message={successMessage} setMessage={setSuccessMessage} />

      <ButtonGroup>
        <Button label="Resetar" type="secondary" onClick={() => {
          const resetForm = document.getElementById('reset');
          const reset = formToJSON(resetForm);

          fetchResetPassword(reset)
            .then(() => {
              setSuccessMessage('Uma nova senha será enviada para o e-mail caso esteja cadastrado.');
            });
        }} />
      </ButtonGroup>
    </div>
  )
};

const mapStateToProps = state => ({
  // isLoggedIn: state.authReducer.isLoggedIn,
  error_message: state.authReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchResetPassword,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ResetarSenha);
