import React from 'react';
import { Punch } from '../../componentes/svg/Punch';
import { usePromiseTracker } from 'react-promise-tracker';
import { Spinner } from '../svg/Spinner';

import './index.css';

export const Button = ({ label, type, children, onClick, className }) => {
  const { promiseInProgress } = usePromiseTracker();
  const defaultName = `button ${type || 'primary'} medium`;

  return promiseInProgress ?
    <span className={`${defaultName} ${className || ''}`}>
      <Spinner height={'24px'} width={'24px'} />
    </span> : (
    <button className={`${defaultName} ${className || ''}`} onClick={onClick}>
      {children}
      <span>{label}</span>
    </button>
  );
}