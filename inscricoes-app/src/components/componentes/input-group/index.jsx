import React from 'react';
import './index.css';

export const InputGroup = ({ children }) => (
  <div className="input-group">
    {children}
  </div>
)
