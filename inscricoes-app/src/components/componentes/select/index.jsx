import React from 'react';

import './index.css';

export const Select = ({ name, label, options, value, onChange }) => (
  <div className="select-div">
    <label htmlFor={name} className="select-label">{label}</label>
    
    <select name={name} id={name} className="select-input" value={value} onChange={(event) => onChange && onChange(event.target.value)}>
      {options.map(option => (
        <option key={option.value} value={option.value}>{option.title}</option>
      ))}
    </select>
  </div>
);
