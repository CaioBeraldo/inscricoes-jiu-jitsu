import React, { useState, useEffect } from 'react';

import './index.scss';

const Message = ({ message, setMessage }) => {
  useEffect(() => {
    setTimeout(() => setMessage(undefined), 5000)
  }, [])

  if (!message) {
    return null;
  }

  return (
    <div className="message-success-message message-success-fade">
      <span>{message}</span>
    </div>
  );
}

export const SuccessMessage = ({ message, setMessage }) => {
  return (
    <div>
      {message ? <Message message={message} setMessage={setMessage} /> : null}
    </div>
  );
}
