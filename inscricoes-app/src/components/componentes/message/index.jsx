import React from 'react';

import './index.scss';

export const Message = ({ message }) => {
  if (!message) {
    return <></>;
  }

  if (typeof(message) !== 'string') {
    message = message
      .map(m => ({ error: m.constraints }))
      .map(m => Object.keys(m.error)
        .map(e => m.error[e])
        .join()
      );
  }

  return (
    <div className="message-error-message">
      {typeof(message) === 'string' ?
        <span className="">{message}</span> :
        message.map((mess, id) => (
            <p key={id} className="">{mess}</p>
          ))}
    </div>
  )
};
