import React, { useState, useEffect } from 'react';
import InputMask from 'react-input-mask';

import './index.scss';

export const Input = ({ type, name, label, value, onChange, mask, maskChar }) => (
  <div className="input-div">
    <label htmlFor={name} className="input-label">
      {label}
    </label>
    <InputMask 
      className="custom-input"
      id={name}
      name={name}
      type={type}
      mask={mask}
      maskChar={maskChar} 
      value={value}
      onChange={(e) => {
        onChange && onChange(e.target.value);
      }}
    />
  </div>
);

export const SearchableInput = ({ name, label, options, value, onChange }) => {
  const [dropdownOptions, setDropdownOptions] = useState([]);
  const [search, setSearch] = useState('');

  const DropdownOptions = ({ dropdownOptions }) => (
    <div id={`dropdown-${name}`} className="dropdown-content">
      {dropdownOptions.map(option => (
        <p key={option.value} onClick={() => {
          document.getElementById(name).value = option.title;
          document.getElementById('dropdown-equipe').style.display = 'none';
        }}>{option.title}</p>
      ))}
    </div>
  );

  useEffect(() => {
    if (search.length === 0) {
      setDropdownOptions([]);
      return;
    }

    const filter = new RegExp(`${search}.*`, 'i');
    const filtered = options.filter(option => !!option.title.match(filter));

    if (!filtered) {
      return;
    }

    setDropdownOptions(filtered);
  }, [search])

  return (
    <div className="input-div">
      <label htmlFor={name} className="input-label">
        {label}
      </label>

      <div className="dropdown">
        <input
          id={name}
          name={name}
          value={search}
          onChange={(e) => {
            setSearch(e.target.value);
          }}
          type="text"
          className="custom-input"
          autoComplete="off" 
        />

        {dropdownOptions.length > 0 ?
          <DropdownOptions dropdownOptions={dropdownOptions} /> :
          null}
      </div>
    </div>
  )
}