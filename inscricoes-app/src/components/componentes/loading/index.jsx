import React from 'react';
import { usePromiseTracker } from 'react-promise-tracker';
import { Spinner } from '../svg/Spinner';

import './index.scss';

export const Loading = () => {
  const { promiseInProgress } = usePromiseTracker();

  return (
    promiseInProgress ? 
      <div className="loading-container">
        <div className="loading-content">
          <div>
            <Spinner />
          </div>
          <label className="loading-label">Carregando...</label>
        </div>
      </div> :
      null
  );
}