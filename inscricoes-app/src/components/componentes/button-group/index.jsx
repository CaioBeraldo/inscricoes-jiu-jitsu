import React from 'react';

import './index.scss'

export const ButtonGroup = ({ children }) => (
  <div className="button-group">
    {children}
  </div>
);
