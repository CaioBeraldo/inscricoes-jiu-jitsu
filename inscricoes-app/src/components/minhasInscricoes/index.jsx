import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { Button } from '../componentes/button';
import { ButtonGroup } from '../componentes/button-group/index';
import { Edit } from '../componentes/svg/Edit';
import { Delete } from '../componentes/svg/Delete';
import { fetchRegistrations } from '../../store/registration/registration.api';

import './index.scss';

const Inscricoes = ({ registrations }) => {
  const [redirectToRegistration, setRedirectToRegistration] = useState(undefined);
  const [redirectToCancelRegistration, setRedirectToCancelRegistration] = useState(undefined);

  if (redirectToRegistration) {
    return <Redirect to={`/inscricao/${redirectToRegistration}`} />
  }

  if (redirectToCancelRegistration) {
    return <Redirect to={`/cancelarInscricao/${redirectToCancelRegistration}`} />
  }

  return (
    registrations.map((obj) => (
      <div className="minhas-incricoes-inscricao" key={obj.id}>
        <h3>{obj.nome}</h3>
        <p>{obj.idade} anos, {obj.peso} kilos, Faixa {obj.faixa.cor}.</p>
        <p>{obj.genero.genero}</p>
        <p>{obj.categoria.nome}</p>
        <p>{obj.absoluto.nome}</p>

        <ButtonGroup>
          <Button label="Cancelar" type="tertiary"
            onClick={() => {
              setRedirectToCancelRegistration(obj.id);
            }}>
            <Delete />
          </Button>
          <Button label="Modificar" type="secondary" onClick={() => {
            setRedirectToRegistration(obj.id);
          }}>
            <Edit fill="white" />
          </Button>
        </ButtonGroup>
      </div>
    ))
  )
};

const MinhasInscricoes = ({ registrations, fetchRegistrations, accessToken }) => {
  useEffect(() => {
    fetchRegistrations(accessToken)
  }, []);

  return (
    <div className="container">
      <div className="titulo">
        <h2>Minhas Inscrições</h2>

        <div className="minhas-incricoes">
          {registrations.length > 0 ?
            <Inscricoes registrations={registrations} /> :
            <h3>Nenhum inscrição realizada...</h3>}
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = state => ({
  accessToken: state.authReducer.accessToken,
  registrations: state.registrationReducer.registrations,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchRegistrations
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MinhasInscricoes);