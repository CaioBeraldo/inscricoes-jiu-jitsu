import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { fetchLogin } from '../../../store/auth/auth.api';
import { formToJSON } from '../../componentes/form/form-to-json';
import { Input } from '../../componentes/input';
import { Button } from '../../componentes/button';
import { ButtonGroup } from '../../componentes/button-group';
import { Message } from '../../componentes/message/index';

import './index.scss';

const Login = ({ fetchLogin, isLoggedIn, error_message }) => {
  if (isLoggedIn) {
    return <Redirect to="/pagamento" />
  }

  return (
    <div className="container">
      <div className="titulo">
        <h2>Acesso ao Painel Administrativo</h2>
      </div>

      <form className="form" id="login">
        <Input type="text" name="username" label="Usuário" />
        <Input type="password" name="password" label="Senha" />
      </form>

      <Message message={error_message} />

      <ButtonGroup>
        <Button label="Acessar" type="secondary" onClick={() => {
          const loginForm = document.getElementById('login');
          const login = formToJSON(loginForm);

          fetchLogin(login);
        }} />
      </ButtonGroup>
    </div>
  )
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  error_message: state.authReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchLogin,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
