import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPayment, fetchRegistrations } from '../../../store/admin';
import { Button } from '../../componentes/button';
import { Check } from '../../componentes/svg/Check';
import { Cancel } from '../../componentes/svg/Cancel';

import './index.scss';

const Pagamento = ({ registrations, fetchPayment, fetchRegistrations }) => {
  useEffect(() => {
    fetchRegistrations();
  }, []);

  const handleFetchPayment = (id, payment) => () => fetchPayment(id, payment).then(fetchRegistrations)

  if (!registrations || (registrations.length === 0)) {
    return <h2 className="pagamento-titulo">Nenhum informação encontrada.</h2>;
  }

  return (
    <div className="container">
      <div>
        <h2 className="pagamento-titulo">
          Pagamento
        </h2>
      </div>

      <div className="pagamento-atletas">
        {registrations.map((registration) => (
          <div key={registration.id} className="pagamento-atletas-atleta">
            <div>
              <span>{registration.nome}</span>
              <span>{registration.equipe.nome}</span>
            </div>
            <div className="icon">
              {registration.pagamentoRealizado ?
                <Check className="pagamento-realizado" /> :
                <Cancel className="pagamento-nao-realizado" />}
            </div>
            <div>
              {!registration.pagamentoRealizado ?
                <Button
                  className='pago'
                  onClick={handleFetchPayment(registration.id, true)}>
                  Pagar
                </Button> :
                <Button
                  className='nao-pago'
                  onClick={handleFetchPayment(registration.id, false)}>
                  Cancelar
                </Button>}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
};

const mapStateToProps = ({ adminReducer: { registrationReducer: { registrations } } }) => ({
  registrations,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchRegistrations,
  fetchPayment,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Pagamento);
