import React from 'react';

import './index.scss';

const Premiacao = () => (
  <div className="container">
    <div className="premiacao">
      <h2>Prêmios:</h2>

      <p>Absoluto Branca<span>R$ 500,00</span></p>
      <p>Absoluto Azul<span>R$ 500,00</span></p>
      <p>Absoluto Roxa<span>R$ 500,00</span></p>
      <p>Absoluto Marrom<span>R$ 500,00</span></p>
      <p>Absoluto Preta<span>R$ 800,00</span></p>

      <p>Absoluto Master/Senior Marrom e Preta<span> R$ 300,00</span></p>
    </div>
  </div>
);

export default Premiacao;
