import React from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserSession } from '../store/auth/auth.api';
import UserRoutes from './rotas/UserRoutes';
import AdminRoutes from './rotas/AdminRoutes';
import { Loading } from '../components/componentes/loading';

import './App.scss';

const App = ({ alterarSenha, isLoggedIn, getUserSession }) => {
  getUserSession();

  return (
    <Router>
      <Switch>
        <Route path="/admin" component={AdminRoutes} />
        <Route path="/" component={UserRoutes} />
      </Switch>
    </Router>
  );
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  alterarSenha: state.authReducer.alterarSenha,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  getUserSession,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
