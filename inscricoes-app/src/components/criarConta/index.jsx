import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input } from '../componentes/input';
import { Select } from '../componentes/select';
import { SearchableInput } from '../componentes/input';
import { Button } from '../componentes/button';
import { InputGroup } from '../componentes/input-group';
import { ButtonGroup } from '../componentes/button-group';
import { fetchCreateUser } from '../../store/users/users.api';
import { fetchTeams } from '../../store/team/team.api';
import { formToJSON } from '../componentes/form/form-to-json';
import { Message } from '../componentes/message/index';

import './index.css';

const CriarConta = ({ teams, error_message, fetchCreateUser, fetchTeams, history }) => {

  useEffect(() => {
    fetchTeams();
  }, []);

  const estados = [
    { title: '', value: '' },
    { title: 'Acre', value: 'AC'},
    { title: 'Alagoas', value: 'AL'},
    { title: 'Amapá', value: 'AP'},
    { title: 'Amazonas', value: 'AM'},
    { title: 'Bahia', value: 'BA'},
    { title: 'Ceará', value: 'CE'},
    { title: 'Distrito Federal', value: 'DF'},
    { title: 'Espírito Santo', value: 'ES'},
    { title: 'Goiás', value: 'GO'},
    { title: 'Maranhão', value: 'MA'},
    { title: 'Mato Grosso', value: 'MT'},
    { title: 'Mato Grosso do Sul', value: 'MS'},
    { title: 'Minas Gerais', value: 'MG'},
    { title: 'Pará', value: 'PA'},
    { title: 'Paraíba', value: 'PB'},
    { title: 'Paraná', value: 'PR'},
    { title: 'Pernambuco', value: 'PE'},
    { title: 'Piauí', value: 'PI'},
    { title: 'Rio de Janeiro', value: 'RJ'},
    { title: 'Rio Grande do Norte', value: 'RN'},
    { title: 'Rio Grande do Sul', value: 'RS'},
    { title: 'Rondônia', value: 'RO'},
    { title: 'Roraima', value: 'RR'},
    { title: 'Santa Catarina', value: 'SC'},
    { title: 'São Paulo', value: 'SP'},
    { title: 'Sergipe', value: 'SE'},
    { title: 'Tocantins', value: 'TO'}
  ];

  return (
    <div className="container">
      <div className="titulo">
        <h2>Entrar</h2>
      </div>

      <form className="form" id="create-user">
        <Input type="text" name="email" label="E-mail" />
        <Input type="password" name="senha" label="Senha" />
        <Input type="text" name='nome' label='Nome' />
        <Input type="text" name="celular" label="Celular" mask="(99) 99999-9999" />
        <InputGroup>
          <Input type="text" name="cidade" label="Cidade" />
          <Select type="text" name="estado" label="Estado" options={estados} />
        </InputGroup>

        <SearchableInput
          name="equipe"
          label="Equipe"
          options={teams.map(team => ({ value: team.id, title: team.nome }))}
          />
      </form>

      <Message message={error_message} />

      <ButtonGroup>
        <Button label="Criar Conta" type="secondary" onClick={() => {
          const createForm = document.getElementById('create-user');
          const user = formToJSON(createForm);

          fetchCreateUser(user)
            .then(() => history.push('/inscricao'));
        }} />
      </ButtonGroup>
    </div>
  );
};

const mapStateToProps = state => ({
  teams: state.teamsReducer.teams,
  error_message: state.usersReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCreateUser,
  fetchTeams,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CriarConta);
