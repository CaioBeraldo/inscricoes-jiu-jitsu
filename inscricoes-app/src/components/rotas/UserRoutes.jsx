import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserSession } from '../../store/auth/auth.api';
import CriarConta from '../criarConta';
import Login from '../login';
import Inscricao from '../inscricao';
import MinhasInscricoes from '../minhasInscricoes';
import Checagem from '../checagem';
import CancelarInscricao from '../cancelarInscricao';
import Home from '../home';
import Localizacao from '../localizacao';
import ValorInscricao from '../valorInscricao';
import Edital from '../edital';
import Premiacao from '../premiacao';
import ResetarSenha from '../resetarSenha';
import AlterarSenha from '../alterarSenha';
import NavigationHeader from '../navigation/UserNavigation';
import InscricoesEncerradas from '../inscricoesEncerradas/index';
import Cronograma from '../cronograma/index';
import Routes from './index';

const PrivateRoutes = () => (
  <>
    <Route path="/inscricao" component={Inscricao} exact />
    <Route path="/inscricao/:id" component={Inscricao} exact />
    <Route path="/minhasInscricoes" component={MinhasInscricoes} exact />
    <Route path="/cancelarInscricao/:id" component={CancelarInscricao} exact />
  </>
);

const RedirectLouggedOut = ({ isLoggedIn }) => (
  !isLoggedIn ? 
    <Redirect to='/' /> :
    <PrivateRoutes />
);

const RedirectSetPassword = ({ alterarSenha }) => {
  if (alterarSenha) {
    return <Redirect to='/alterarSenha' />;
  }

  return null;
};

const UserRoutes = ({ alterarSenha, isLoggedIn, getUserSession }) => {
  return (
    <>
      <Routes basename="/" Header={NavigationHeader}>
        <Route path="/" component={Home} exact />
        <Route path="/checagem" component={Checagem} exact />
        <Route path="/entrar" component={InscricoesEncerradas /*Login*/} exact />
        <Route path="/criarConta" component={InscricoesEncerradas /*CriarConta*/} exact />
        <Route path="/valorInscricao" component={ValorInscricao} exact />
        <Route path="/premiacao" component={Premiacao} exact />
        <Route path="/localEvento" component={Localizacao} exact />
        <Route path="/edital" component={Edital} exact />
        <Route path="/cronograma" component={Cronograma} exact />
        <Route path="/resetarSenha" component={InscricoesEncerradas /*ResetarSenha*/} exact />
        <Route path="/alterarSenha" component={InscricoesEncerradas /*AlterarSenha*/} exact />

        <RedirectLouggedOut isLoggedIn={isLoggedIn} />
        <RedirectSetPassword alterarSenha={alterarSenha} />
      </Routes>
    </>
  );
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  alterarSenha: state.authReducer.alterarSenha,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  getUserSession,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserRoutes);
