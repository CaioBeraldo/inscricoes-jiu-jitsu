import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserSession } from '../../store/auth/auth.api';
import Routes from './index';
import Pagamento from '../admin/pagamento';
import NavigationHeader from '../navigation/AdminNavigation';

import Login from '../admin/login';

const PrivateRoutes = () => (
  <>
    {/*<Route path="/inscricao" component={Inscricao} exact />
    <Route path="/inscricao/:id" component={Inscricao} exact />
    <Route path="/minhasInscricoes" component={MinhasInscricoes} exact />
    <Route path="/cancelarInscricao/:id" component={CancelarInscricao} exact />*/}
  </>
);

const RedirectLouggedOut = ({ isLoggedIn }) => (
  !isLoggedIn ? 
    <Redirect to='/' /> :
    <PrivateRoutes />
);

const AdminRoutes = ({ isLoggedIn }) => {
  return (
    <Routes basename="/admin" Header={NavigationHeader}>
      <Route path="/" component={Login} exact />
      <Route path="/pagamento" component={Pagamento} exact />
      {/*<Route path="/chave" component={Chave} exact /> */}

      <RedirectLouggedOut isLoggedIn={isLoggedIn} />
    </Routes>
  );
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  alterarSenha: state.authReducer.alterarSenha,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  getUserSession,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoutes);
