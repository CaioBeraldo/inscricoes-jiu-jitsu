import React from 'react';
import { HashRouter as Router, Route, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Loading } from '../../components/componentes/loading';
import Navigation from '../navigation/index';

const Routes = ({ children, basename, Header }) => {
  return (
    <Router basename={basename}>
      <Navigation>
        <Header />
      </Navigation>

      <div className="navigation-content">
        {children}
      </div>

      <Loading />
    </Router>
  );
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  alterarSenha: state.authReducer.alterarSenha,
});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
