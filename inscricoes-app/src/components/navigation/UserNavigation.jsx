import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logOut } from '../../store/auth/auth.api';

const closeMenu = () => {
  const menuToggle = document.getElementById('navigation-toggle-menu');
  menuToggle.checked = false;
}

const PublicHeader = () => (
  <>
    <li><Link onClick={closeMenu} to="/">Início</Link></li>
    <li><Link onClick={closeMenu} to="/checagem">Checagem</Link></li>
    <li><Link onClick={closeMenu} to="/valorInscricao">Valores</Link></li>
    <li><Link onClick={closeMenu} to="/premiacao">Prêmios</Link></li>
    <li><Link onClick={closeMenu} to="/localEvento">Local do Evento</Link></li>
    <li><Link onClick={closeMenu} to="/edital">Edital</Link></li>
    <li><Link onClick={closeMenu} to="/cronograma">Cronograma</Link></li>
  </>
);

const NavigationHeader = ({ logOut, isLoggedIn }) => {
  if (!isLoggedIn) {
    return (
      <> 
        <PublicHeader />
        <li><Link onClick={closeMenu} to="/entrar">Entrar</Link></li>
      </>
    );
  }

  return (
    <>
      <PublicHeader />
      <li><Link onClick={closeMenu} to="/inscricao">Inscrição</Link></li>
      <li><Link onClick={closeMenu} to="/minhasInscricoes">Minhas Inscrições</Link></li>
      <li><Link onClick={logOut} to="/">Sair</Link></li>
    </>
  )
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  logOut,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NavigationHeader);
