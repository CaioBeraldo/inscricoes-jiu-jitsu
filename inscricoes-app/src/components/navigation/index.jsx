import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logOut } from '../../store/auth/auth.api';
import Logo from '../../assets/logo.png';

import './index.scss';

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  logOut,
}, dispatch);

const Navigation = ({ children }) => {
  return (
    <nav role="navigation" className="navigation">
      <div className="navigation-header">
        <img src={Logo} alt="Logo Liga Regional de Jiu Jitsu" className="navigation-logo" />
      </div>

      <div className="navigation-toggle">
        <input id="navigation-toggle-menu" type="checkbox" />

        <span></span>
        <span></span>
        <span></span>

        <ul>
          {children}
        </ul>
      </div>
    </nav>
  )
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
