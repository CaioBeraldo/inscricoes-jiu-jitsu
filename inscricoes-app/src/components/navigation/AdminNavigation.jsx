import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logOut } from '../../store/auth/auth.api';

const closeMenu = () => {
  const menuToggle = document.getElementById('navigation-toggle-menu');
  menuToggle.checked = false;
}

const NavigationHeader = ({ roles, logOut, isLoggedIn }) => {
  if (!isLoggedIn) {
    return <li><Link onClick={closeMenu} to="/">Entrar</Link></li>;
  }

  return (
    <>
      <li><Link onClick={closeMenu} to="/pagamento">Pagamentos</Link></li>
      <li><Link onClick={() => {
        logOut();
        closeMenu();
      }} to="/">Sair</Link></li>
    </>
  )
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  roles: state.authReducer.roles,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  logOut,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NavigationHeader);
