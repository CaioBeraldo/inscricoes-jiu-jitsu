import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input } from '../componentes/input';
import { Message } from '../componentes/message';
import { ButtonGroup } from '../componentes/button-group';
import { Button } from '../componentes/button';
import { formToJSON } from '../componentes/form/form-to-json';
import { fetchUpdatePassword } from '../../store/users/users.api';

import './index.scss';

const AlterarSenha = ({ accessToken, fetchUpdatePassword, history, error_message }) => (
  <div className="container">
    <div className="titulo">
        <h2>Informe uma nova Senha</h2>
      </div>

      <form className="form" id="reset">
        <Input type="password" name="senha" label="Senha" />
      </form>

      <Message message={error_message} />

      <ButtonGroup>
        <Button label="Alterar" type="secondary" onClick={() => {
          const resetForm = document.getElementById('reset');
          const reset = formToJSON(resetForm);

          fetchUpdatePassword(accessToken, reset)
            .then(() => history.push('/minhasInscricoes'))
        }} />
      </ButtonGroup>
  </div>
);

const mapStateToProps = state => ({
  accessToken: state.authReducer.accessToken,
  error_message: state.usersReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchUpdatePassword,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AlterarSenha);
