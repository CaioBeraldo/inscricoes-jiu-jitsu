import React from 'react';
import { SPONSORS } from './sponsors';

import './index.scss';

const ShowSponsorships = () => (
  <>
    {SPONSORS.map((file, id) => (
      <div className="sponsorship-slide" key={id}>
        <div className="sponsorship-image">
          <img 
            src={file.src}
            height={100}
            width={file.width || 250}
            alt={'file.name'} />
        </div>
      </div>
    ))}
  </>
);

export const Sponsorship = () => (
	<div className="sponsorship">
		<div className="sponsorship-slider">
			<div className="sponsorship-slide-track">
        <ShowSponsorships />
        <ShowSponsorships />
			</div>
		</div>
	</div>
);