import ALVO from '../../assets/patrocinadores/ALVO.png'
import AQUAMARINE from '../../assets/patrocinadores/AQUAMARINE.png'
import CAROZITA from '../../assets/patrocinadores/CAROZITA.png'
import CAV from '../../assets/patrocinadores/CAV.png'
import CORRETIVA from '../../assets/patrocinadores/CORRETIVA.png'
import DEAM from '../../assets/patrocinadores/DEAM.png'
import DK from '../../assets/patrocinadores/DK.png'
import DREAM from '../../assets/patrocinadores/DREAM.png'
import ECOTAG from '../../assets/patrocinadores/ECOTAG.png'
import FORCA_MAXIMA from '../../assets/patrocinadores/FORCA_MAXIMA.png'
import GAS from '../../assets/patrocinadores/GAS.png'
import INTERCELL from '../../assets/patrocinadores/INTERCELL.png'
import INTERIOR from '../../assets/patrocinadores/INTERIOR.png'
import JOSE from '../../assets/patrocinadores/JOSE.png'
import LOPES from '../../assets/patrocinadores/LOPES.png'
import MA_SPORT from '../../assets/patrocinadores/MA_SPORT.png'
import MAXTITANIUM from '../../assets/patrocinadores/MAXTITANIUM.png'
import MKYS from '../../assets/patrocinadores/MKYS.png'
import NET_FIT from '../../assets/patrocinadores/NET_FIT.png'
import NUAGE from '../../assets/patrocinadores/NUAGE.png'
import OTICA_VEJA from '../../assets/patrocinadores/OTICA_VEJA.png'
import PECADO_ACAI from '../../assets/patrocinadores/PECADO_ACAI.png'
import POUPAR from '../../assets/patrocinadores/POUPAR.png'
import RICARDO from '../../assets/patrocinadores/RICARDO.png'
import SERJ from '../../assets/patrocinadores/SERJ.png'
import TELEPIZZA from '../../assets/patrocinadores/TELEPIZZA.png'
import TRATA from '../../assets/patrocinadores/TRATA.png'
import unimed from '../../assets/patrocinadores/unimed.png'

export const SPONSORS = [
  { src: ALVO, width: 200 },
  { src: AQUAMARINE },
  { src: CAROZITA },
  { src: CAV, width: 150, },
  { src: CORRETIVA, width: 110, },
  { src: DEAM, width: 200 },
  { src: DK, width: 110, },
  { src: DREAM, width: 140, },
  { src: ECOTAG },
  { src: FORCA_MAXIMA, width: 150, },
  { src: GAS, width: 190, },
  { src: INTERCELL },
  { src: INTERIOR },
  { src: JOSE },
  { src: LOPES },
  { src: MA_SPORT, width: 110, },
  { src: MAXTITANIUM },
  { src: MKYS, width: 150, },
  { src: NET_FIT },
  { src: NUAGE },
  { src: OTICA_VEJA, width: 200 },
  { src: PECADO_ACAI, width: 150 },
  { src: POUPAR },
  { src: RICARDO, width: 110 },
  { src: SERJ },
  { src: TELEPIZZA },
  { src: TRATA },
  { src: unimed },
];
