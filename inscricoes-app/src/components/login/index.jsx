import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, Redirect } from 'react-router-dom';
import { Input } from '../componentes/input';
import { Button } from '../componentes/button';
import { ButtonGroup } from '../componentes/button-group';
import { fetchLogin } from '../../store/auth/auth.api';
import { formToJSON } from '../componentes/form/form-to-json';
import { Message } from '../componentes/message/index';

import './index.css';

const Login = ({ fetchLogin, isLoggedIn, error_message }) => {
  if (isLoggedIn) {
    return <Redirect to="/minhasInscricoes" />
  }

  return (
    <div className="container">
      <div className="titulo">
        <h2>Entrar</h2>
      </div>

      <form className="form" id="login">
        <Input type="text" name="username" label="E-mail" />
        <Input type="password" name="password" label="Senha" />
      </form>

      <Message message={error_message} />

      <ButtonGroup>
        <Button label="Entrar" type="secondary" onClick={() => {
          const loginForm = document.getElementById('login');
          const login = formToJSON(loginForm);

          fetchLogin(login);
        }} />
      </ButtonGroup>

      <Link to="criarConta" className="link">Criar Conta</Link>
      <Link to="resetarSenha" className="link">Esqueci minha senha</Link>
    </div>
  )
};

const mapStateToProps = state => ({
  isLoggedIn: state.authReducer.isLoggedIn,
  error_message: state.authReducer.error_message,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchLogin,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
