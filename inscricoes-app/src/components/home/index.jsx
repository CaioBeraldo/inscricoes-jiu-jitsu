import React from 'react';
import { Link } from 'react-router-dom';
import Banner from '../../assets/capa.jpg';
import { Sponsorship } from '../sponsorship';

import './index.scss';

const Home = () => (
  <div className="container home-conteiner">
    {/*<div>
      <Link to="/entrar" className="link home-atention">Clique aqui para fazer sua inscrição</Link>
    </div>*/}

    <div className="home-banner">
      <img src={Banner} className="home-banner-image" alt="Banner Liga Regional de Jiu Jitsu" />
    </div>

    <Sponsorship />
  </div>
);

export default Home;
