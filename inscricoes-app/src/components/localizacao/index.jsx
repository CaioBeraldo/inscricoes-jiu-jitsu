import React from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

import './index.scss';

export const Localizacao = ({ google }) => {
  const mapStyles = {
    position: 'relative',
    width: '250px',
    height: '250px',
  };

  const localizacao = {
    lat: -22.201331,
    lng: -49.960741,
  };

  return (
    <div className="container">
      <div className="localizacao">
        <Map
          google={google}
          zoom={17}
          style={mapStyles}
          initialCenter={localizacao}>
          <Marker position={localizacao} />
        </Map>
      </div>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: 'AIzaSyArE4lhRIU6Ut3mk8Rs2PwLXQqSt4_c3_0'
})(Localizacao);
