import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCheck, fetchCheckAbsolute } from '../../store/check/check.api';
import { fetchCategories } from '../../store/categories/categories.api';
import { fetchAbsolutes } from '../../store/absolutes/absolutes.api';
import { Select } from '../componentes/select';
import { usePromiseTracker } from 'react-promise-tracker';

import './index.scss';

const Competidores = ({ checks }) => (
  checks
    .sort((atleta1, atleta2) => atleta1.equipe.nome > atleta2.equipe.nome ? 1 : -1)
    .map(check => (
      <div key={check.id}>
        <h3>
          {check.nome}
          <span>{check.equipe.nome}</span>
        </h3>
      </div>
    ))
);

const Checagem = ({ checks, categories, absolutes, fetchCheck, fetchCheckAbsolute, fetchCategories, fetchAbsolutes }) => {
  const [category, setCategory] = useState(undefined);
  const [categorias, setCategorias] = useState([]);

  const { promiseInProgress } = usePromiseTracker();

  useEffect(() => {
    fetchCategories()
      .then(() => fetchAbsolutes())
      .then(() => {
        setCategorias([
          { value: 0, title: '' },
          ...[
            ...categories.map(c => ({ value: c.id, title: c.nome, type: 0 })),
            ...absolutes.map(a => ({ value: a.id, title: a.nome, type: 1 }))
          ].sort((c1, c2) => c1.title > c2.title ? 1 : -1)
        ]);
      });
  }, []);

  useEffect(() => {
    if (!category) {
      return;
    }

    const value = JSON.parse(category);

    if (value.type === 0) {
      fetchCheck(value.id);
    } else {
      fetchCheckAbsolute(value.id);
    }
  }, [category]);

  const getCategories = () => [
    { value: 0, title: '' },
    ...[
      ...categories.map(c => ({ value: JSON.stringify({ id: c.id, type: 0 }), title: c.nome })),
      ...absolutes.map(a => ({ value: JSON.stringify({ id: a.id, type: 1 }), title: a.nome }))
    ].sort((c1, c2) => c1.title > c2.title ? 1 : -1)
  ];

  return (
    <div className="container">
      <div className="titulo">  
        <h2>Checagem do Campeonato</h2>
        <h2 className="color-secondary">Great Champions</h2>
      </div>

      <div className="checagem-menu">
        <Select
          name="categoria" 
          label="Selecione sua categoria"
          value={category}
          onChange={setCategory}
          options={getCategories()} />
      </div>

      <div className="checagem-inscricao">
        {checks.length > 0 ?
          <Competidores checks={checks} /> :
            promiseInProgress ? 
              <h3>Carregando...</h3> :
              <h3>Nenhuma inscrição realizada para a categoria...</h3>}
      </div>
    </div>
  )
};

const mapStateToProps = state => ({
  accessToken: state.authReducer.accessToken,
  checks: state.checkReducer.checks,
  categories: state.categoriesReducer.categories,
  absolutes: state.absolutesReducer.absolutes,
});
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchCheck,
  fetchCheckAbsolute,
  fetchCategories,
  fetchAbsolutes,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Checagem);