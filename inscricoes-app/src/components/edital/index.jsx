import React from 'react';
import regulamento from '../../assets/edital.pdf';

import './index.scss';

const Edital = () => (
  <div className="container">
    <iframe src={regulamento} className="edital">
    </iframe>
  </div>
);

export default Edital;
