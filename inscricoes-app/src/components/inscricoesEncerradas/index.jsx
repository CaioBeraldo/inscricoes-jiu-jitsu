import React from 'react';

import './index.scss';

const InscricoesEncerradas = () => (
  <div className="container home-conteiner">
    <h3 className="inscricoes-encerradas-h3">As inscrições para o campeonato foram encerradas.</h3>
  </div>
);

export default InscricoesEncerradas;
