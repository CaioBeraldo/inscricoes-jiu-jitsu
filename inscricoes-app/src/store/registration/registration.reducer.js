import { REGISTRATION_API } from './registration.types';

const DEFAULT = {
  error_message: undefined,
  registrations: [],
};

const registrationReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case REGISTRATION_API.FETCH_REGISTRATION_SUCCESS:
      return { ...state, error_message: undefined };
    case REGISTRATION_API.FETCH_REGISTRATION_ERROR:
      return { ...state, error_message: action.error_message };
    case REGISTRATION_API.FETCH_GET_REGISTRATIONS_SUCCESS:
      return { ...state, error_message: undefined, registrations: action.registrations };
    case REGISTRATION_API.FETCH_GET_REGISTRATIONS_ERROR:
      return { ...state, error_message: action.error_message };
    default:
      return state;
  }
};

export default registrationReducer;
