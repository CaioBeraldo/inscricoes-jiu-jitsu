import { options, handleResponse, handlerErrors, fetchAPI } from '../handlers.api';
import { trackPromise } from 'react-promise-tracker';
import { REGISTRATION_API } from './registration.types';
import { API_URL } from '../../constants';

const registrationApiUrl = `${API_URL}/registrations`;

const fetchRegisterSuccess = () => ({
  type: REGISTRATION_API.FETCH_REGISTRATION_SUCCESS,
});

const fetchRegisterError = ({ message }) => ({
  type: REGISTRATION_API.FETCH_REGISTRATION_ERROR,
  error_message: message,
});

export const fetchRegister = (registration, accessToken) => (dispatch) => {
  const requestOptions = {
    ...options,
    method: 'POST',
    headers: {
      ...options.headers,
      Authorization: `Bearer ${accessToken}`
    },
    body: JSON.stringify(registration),
  }

  return fetchAPI(registrationApiUrl, requestOptions)
    .then(registration => dispatch(fetchRegisterSuccess()))
    .catch(error => {
      dispatch(fetchRegisterError(error));
      return Promise.reject(error);
    })
};

export const fetchUpdateRegistration = (registration, accessToken) => (dispatch) => {
  const requestOptions = {
    ...options,
    method: 'PUT',
    headers: {
      ...options.headers,
      Authorization: `Bearer ${accessToken}`
    },
    body: JSON.stringify(registration),
  }

  return fetchAPI(`${registrationApiUrl}/${registration.id}`, requestOptions)
    .then(registration => dispatch(fetchRegisterSuccess()))
    .catch(error => {
      dispatch(fetchRegisterError(error));
      return Promise.reject(error);
    })
};

const fetchRegistrationsSuccess = (registrations) => ({
  type: REGISTRATION_API.FETCH_GET_REGISTRATIONS_SUCCESS,
  registrations,
});

const fetchRegistrationsError = ({ message }) => ({
  type: REGISTRATION_API.FETCH_GET_REGISTRATIONS_ERROR,
  error_message: message,
});

export const fetchRegistrations = (accessToken) => (dispatch) => {
  const requestOptions = {
    ...options,
    headers: {
      ...options.headers,
      Authorization: `Bearer ${accessToken}`
    }
  }

  return fetchAPI(registrationApiUrl, requestOptions)
    .then(registrations => dispatch(fetchRegistrationsSuccess(registrations)))
    .catch(error => {
      dispatch(fetchRegistrationsError(error));
      return Promise.reject(error);
    })
};

const fetchDeleteRegistrationSuccess = (checks) => ({
  type: REGISTRATION_API.FECH_GET_CHECK_SUCCESS,
  checks,
});

const fetchDeleteRegistrationError = ({ message }) => ({
  type: REGISTRATION_API.FECH_GET_CHECK_ERROR,
  error_message: message,
});

export const fetchDeleteRegistration = (id, accessToken) => (dispatch) => {
  return fetchAPI(`${registrationApiUrl}/${id}`, { 
      ...options,
      method: 'DELETE',
      headers: {
        ...options.headers,
        Authorization: `Bearer ${accessToken}`
      },
    })
    .then(checks => dispatch(fetchDeleteRegistrationSuccess(checks)))
    .catch(error => {
      dispatch(fetchDeleteRegistrationError(error));
      return Promise.reject(error);
    })
};
