import { TEAM_API } from './team.types';

const DEFAULT = {
  error_message: undefined,
  teams: [],
};

const teamsReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case TEAM_API.FETCH_TEAM_SUCCESS:
      return { ...state, teams: action.teams, error_message: undefined };
    case TEAM_API.FETCH_TEAM_ERROR:
      return { ...state, error_message: action.error_message };
    default:
      return state;
  }
};

export default teamsReducer;
