import { TEAM_API } from './team.types';
import { options, handlerErrors, handleResponse, fetchAPI } from '../handlers.api';
import { API_URL } from '../../constants';

const teamApiUrl = `${API_URL}/team`;

const fetchTeamsSuccess = (teams) => ({
  type: TEAM_API.FETCH_TEAM_SUCCESS,
  teams,
});

const fetchTeamsError = ({ error_message }) => ({
  type: TEAM_API.FETCH_TEAM_ERROR,
  error_message
});

export const fetchTeams = () => (dispatch) => {
  return fetchAPI(teamApiUrl, { ...options })
    .then(teams => dispatch(fetchTeamsSuccess(teams)))
    .catch(error => {
      dispatch(fetchTeamsError(error))
      return Promise.reject(error);
    })
};
