import { trackPromise } from 'react-promise-tracker';

export const handlerErrors = (data) => {
  if ((data.error) || ([400, 401].some(val => val === data.statusCode))) {
    const error = { message: data.message || data.error};
    return Promise.reject(error);
  }

  return Promise.resolve(data);
};

export const handleResponse = (response) => {
  return response.json();
};

export const options = {
  "method": "GET",
  "crossDomain": true,
  "headers": {
    "Content-Type": "application/json",
    "Accept": "*/*",
  }
};

export const fetchAPI = (url, options) => {
  return trackPromise(
    fetch(url, options)
      .then(handleResponse)
      .then(handlerErrors)
  );
}
