import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import authReducer from './auth/auth.reducer';
import usersReducer from './users/users.reducer';
import registrationReducer from './registration/registration.reducer';
import teamsReducer from './team/team.reducer';
import categoriesReducer from './categories/categories.reducer';
import absolutesReducer from './absolutes/absolutes.reducer';
import checkReducer from './check/check.reducer';
import adminReducer from './admin';

const reducers = combineReducers({
  authReducer,
  usersReducer,
  registrationReducer,
  teamsReducer,
  categoriesReducer,
  absolutesReducer,
  checkReducer,
  adminReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

// store.subscribe(() => { console.log(store.getState()); });

export default store;
