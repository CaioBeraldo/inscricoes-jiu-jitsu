import { CATEGORY_API } from './categories.type';

const DEFAULT = {
  categories: [],
};

const categoriesReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case CATEGORY_API.FETCH_CATEGORIES_SUCCESS:
      return { ...state, categories: action.categories };
    default:
      return state;
  }
};

export default categoriesReducer;
