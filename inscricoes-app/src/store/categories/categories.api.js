import { fetchAPI, options } from '../handlers.api';
import { API_URL } from '../../constants';
import { CATEGORY_API } from './categories.type';

const fetchCategoriesSuccess = (categories) => ({
  type: CATEGORY_API.FETCH_CATEGORIES_SUCCESS,
  categories,
});

export const fetchCategories = () => (dispatch) => 
  fetchAPI(`${API_URL}/category`, options)
    .then(categories => dispatch(fetchCategoriesSuccess(categories)));
