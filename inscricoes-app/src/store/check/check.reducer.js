import { CHECK_API } from './check.types';

const DEFAULT = {
  checks: []
};

const checkReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case CHECK_API.FECH_GET_CHECK_SUCCESS:
      return { ...state, error_message: undefined, checks: action.checks };
    case CHECK_API.FECH_GET_CHECK_ERROR:
      return { ...state, error_message: action.error_message };
    default:
      return state;
  }
};

export default checkReducer;
