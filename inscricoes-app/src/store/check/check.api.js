import { options, handleResponse, handlerErrors, fetchAPI } from '../handlers.api';
import { CHECK_API } from './check.types';
import { API_URL } from '../../constants';

const checkApiUrl = `${API_URL}/check`;

const fetchCheckSuccess = (checks) => ({
  type: CHECK_API.FECH_GET_CHECK_SUCCESS,
  checks,
});

const fetchCheckError = ({ message }) => ({
  type: CHECK_API.FECH_GET_CHECK_ERROR,
  error_message: message,
});

export const fetchCheck = (category) => (dispatch) => {
  return fetchAPI(`${checkApiUrl}/category/${category}`, { ...options })
    .then(checks => dispatch(fetchCheckSuccess(checks)))
    .catch(error => {
      dispatch(fetchCheckError(error));
      return Promise.reject(error);
    })
};

export const fetchCheckAbsolute = (absolute) => (dispatch) => {
  return fetchAPI(`${checkApiUrl}/absolute/${absolute}`, { ...options })
    .then(checks => dispatch(fetchCheckSuccess(checks)))
    .catch(error => {
      dispatch(fetchCheckError(error));
      return Promise.reject(error);
    })
};
