import { fetchAPI, options } from '../handlers.api';
import { API_URL } from '../../constants';
import { ABSOLUTE_API } from './absolutes.type';

const fetchAbsolutesSuccess = (absolutes) => ({
  type: ABSOLUTE_API.FETCH_ABSOLUTES_SUCCESS,
  absolutes,
});

export const fetchAbsolutes = () => (dispatch) => 
  fetchAPI(`${API_URL}/absolute`, options)
    .then(absolutes => dispatch(fetchAbsolutesSuccess(absolutes)));
