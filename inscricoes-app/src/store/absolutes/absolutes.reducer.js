import { ABSOLUTE_API } from './absolutes.type';

const DEFAULT = {
  absolutes: [],
};

const absolutesReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ABSOLUTE_API.FETCH_ABSOLUTES_SUCCESS:
      return { ...state, absolutes: action.absolutes };
    default:
      return state;
  }
};

export default absolutesReducer;
