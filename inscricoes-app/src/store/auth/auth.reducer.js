import { AUTH_API, GET_USER_SESSION, LOG_OUT } from './auth.types';

const DEFAULT = {
  isLoggedIn: false,
  alterarSenha: false,
  accessToken: undefined,
  error_message: undefined,
};

const rootReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case AUTH_API.FETCH_LOGIN_SUCCESS:
      sessionStorage.setItem('accessToken', action.access_token)
      return {
        ...state,
        isLoggedIn: true,
        accessToken: action.access_token,
        alterarSenha: action.alterarSenha,
        email: action.email,
        error_message: undefined,
      };
    case AUTH_API.FETCH_LOGIN_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        accessToken: undefined,
        alterarSenha: false,
        email: undefined,
        error_message: action.error_message,
      };
    case AUTH_API.FETCH_RESET_PASSWORD_SUCCESS:
      return { ...state, error_message: undefined };
    case AUTH_API.FETCH_RESET_PASSWORD_ERROR:
      return { ...state, error_message: action.error_message };
    case GET_USER_SESSION:
      const accessToken = sessionStorage.getItem('accessToken');
      return { ...state, accessToken, isLoggedIn: !!accessToken };
    case LOG_OUT:
        sessionStorage.removeItem('accessToken');
        return { ...state, accessToken: undefined, isLoggedIn: false };
    default:
      return state;
  }
};

export default rootReducer
