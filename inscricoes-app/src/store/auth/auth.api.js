
import { options, fetchAPI } from '../handlers.api';
import { AUTH_API, GET_USER_SESSION, LOG_OUT } from './auth.types';
import { API_URL } from '../../constants';

const authApiUrl = `${API_URL}/auth`;

export const getUserSession = () => ({
  type: GET_USER_SESSION,
  access_token: sessionStorage.getItem('accessToken'),
});

export const logOut = () => ({
  type: LOG_OUT,
});

const fetchLoginSuccess = ({ access_token, alterarSenha, email }) => ({
  type: AUTH_API.FETCH_LOGIN_SUCCESS,
  access_token,
  alterarSenha,
  email,
});

const fetchLoginError = ({ message }) => ({
  type: AUTH_API.FETCH_LOGIN_ERROR,
  error_message: message
});

export const fetchLogin = (login) => (dispatch) => {
  return fetchAPI(authApiUrl, {
      ...options,
      method: 'POST',
      body: JSON.stringify(login)
    })
    .then(login => dispatch(fetchLoginSuccess(login)))
    .catch(error => {
      dispatch(fetchLoginError(error));
      return Promise.reject(error);
    })
};

const fetchResetPasswordSuccess = () => ({
  type: AUTH_API.FETCH_RESET_PASSWORD_SUCCESS,
});

const fetchResetPasswordError = ({ message }) => ({
  type: AUTH_API.FETCH_RESET_PASSWORD_ERROR,
  error_message: message
});

export const fetchResetPassword = (reset) => (dispatch) => {
  return fetchAPI(authApiUrl, {
      ...options,
      method: 'PUT',
      body: JSON.stringify(reset)
    })
    .then(reset => dispatch(fetchResetPasswordSuccess(reset)))
    .catch(error => {
      dispatch(fetchResetPasswordError(error));
      return Promise.reject(error);
    })
};
