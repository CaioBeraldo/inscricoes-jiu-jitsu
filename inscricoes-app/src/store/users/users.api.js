
import { options, fetchAPI } from '../handlers.api';
import { USER_API } from './users.types';
import { fetchLogin } from '../auth/auth.api';
import { API_URL } from '../../constants';

const usersApiUrl = `${API_URL}/users`;

const fetchCreateUserSuccess = () => ({
  type: USER_API.FETCH_CREATE_USER_SUCCESS,
});

const fetchCreateUserError = ({ message }) => ({
  type: USER_API.FETCH_CREATE_USER_ERROR,
  error_message: message,
});

export const fetchCreateUser = (user) => (dispatch) => {
  return fetchAPI(usersApiUrl, {
      ...options,
      method: 'POST',
      body: JSON.stringify(user)
    })
    .then(user_created => fetchCreateUserSuccess())
    .then(() => dispatch(fetchLogin({
      username: user.email,
      password: user.senha,
    })))
    .catch(error => {
      dispatch(fetchCreateUserError(error))
      return Promise.reject(error);
    })
};

export const fetchUpdatePassword = (accessToken, reset) => (dispatch) => {
  return fetchAPI(usersApiUrl, {
      ...options,
      method: 'PUT',
      headers: {
        ...options.headers,
        Authorization: `Bearer ${accessToken}`
      },
      body: JSON.stringify(reset),
    })
    .then((user) => {
      return dispatch(fetchLogin({
        username: user.email,
        password: reset.senha,
      }))
    })
    .catch(error => {
      dispatch(fetchCreateUserError(error));
      return Promise.reject(error);
    })
};