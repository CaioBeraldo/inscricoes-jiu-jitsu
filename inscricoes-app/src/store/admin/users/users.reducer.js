import { USER_API } from './users.types';

const DEFAULT = {
  error_message: undefined,
}

const rootReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case USER_API.FETCH_CREATE_USER_SUCCESS:
      return { ...state, error_message: undefined };
    case USER_API.FETCH_CREATE_USER_ERROR:
      return { ...state, error_message: action.error_message };
    default:
      return state;
  }
};

export default rootReducer
