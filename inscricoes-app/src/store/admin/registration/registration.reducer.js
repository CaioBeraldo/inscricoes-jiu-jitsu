import { REGISTRATION_API } from './registration.types';

const DEFAULT = {
  error_message: undefined,
  registrations: [],
};

const registrationReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case REGISTRATION_API.FETCH_REGISTRATIONS_SUCCESS:
      return { ...state, registrations: action.registrations, error_message: undefined };
    case REGISTRATION_API.FETCH_REGISTRATIONS_ERROR:
      return { ...state, registrations: action.registrations, error_message: action.error_message };
    default:
      return state;
  }
};

export default registrationReducer;
