import { options, fetchAPI } from '../../handlers.api';
import { REGISTRATION_API } from './registration.types';
import { API_URL } from '../../../constants';

const registrationApiUrl = `${API_URL}/admin/registrations`;

const fetchRegistrationsSuccess = (registrations) => ({
  type: REGISTRATION_API.FETCH_REGISTRATIONS_SUCCESS,
  registrations,
});

const fetchRegistrationsError = ({ message }) => ({
  type: REGISTRATION_API.FETCH_REGISTRATIONS_ERROR,
  error_message: message,
});

export const fetchRegistrations = (/*accessToken*/) => (dispatch) => {
  return fetchAPI(`${registrationApiUrl}`, { 
      ...options,
      headers: {
        ...options.headers,
        Authorization: `Bearer ${sessionStorage.accessToken}`
      },
    })
    .then(registrations => dispatch(fetchRegistrationsSuccess(registrations)))
    .catch(error => {
      dispatch(fetchRegistrationsError(error));
      return Promise.reject(error);
    });
};

const fetchPaymentSuccess = (registrations) => ({
  type: REGISTRATION_API.FETCH_PAYMENT_SUCCESS,
  registrations,
});

const fetchPaymentError = ({ message }) => ({
  type: REGISTRATION_API.FETCH_PAYMENT_ERROR,
  error_message: message,
});

export const fetchPayment = (id, payment /*accessToken*/) => (dispatch) => {
  return fetchAPI(`${registrationApiUrl}/${id}`, { 
      ...options,
      method: 'PUT',
      headers: {
        ...options.headers,
        Authorization: `Bearer ${sessionStorage.accessToken}`
      },
      body: JSON.stringify({ pagamentoRealizado: payment })
    })
    .then(registrations => dispatch(fetchPaymentSuccess(registrations)))
    .catch(error => {
      dispatch(fetchPaymentError(error));
      return Promise.reject(error);
    });
};
