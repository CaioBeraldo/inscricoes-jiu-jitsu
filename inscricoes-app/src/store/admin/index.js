import { combineReducers } from 'redux';
import usersReducer from './users/users.reducer';
import registrationReducer from './registration/registration.reducer';
import { fetchPayment, fetchRegistrations } from './registration/registration.api';
import { fetchUpdatePassword, fetchCreateUser } from './users/users.api';

const adminReducer = combineReducers({
  usersReducer,
  registrationReducer,
});

export { fetchPayment, fetchRegistrations, fetchUpdatePassword, fetchCreateUser };

export default adminReducer;
