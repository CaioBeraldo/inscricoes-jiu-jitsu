
let url = 'http://localhost:3000'
if (process.env.NODE_ENV === 'production') {
  url = 'https://lrjj.azurewebsites.net'
}

export const API_URL = url;
